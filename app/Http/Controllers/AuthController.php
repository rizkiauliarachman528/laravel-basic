<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InvalidArgumentException;

class AuthController extends Controller
{
    function index(Request $req) {
        // return dd($req->all());
        try {
            // untuk mencari data USER berdasarkan EMAIL
            $find = User::where('email', $req->email)->first();

            // return dd($find);

            /**
             * untuk mengecek apa USER ditemukan atau tidak  
             * jika tidak ditemukan akan muncul notifikasi di bawah 
             */
            if (!$find) {
                throw new InvalidArgumentException("Akun tidak ditemukan", 500); 
            }

            if (!Hash::check($req->password, $find->password)) {
                throw new InvalidArgumentException('Email atau password salah!', 500);
            }

            /**
             * Auth:: untuk memberi aksen login
             * sebagai web
             */
            Auth::guard('web')->login($find, $remember = true);
            
            /**
             * untuk otomatis berpindah ke halaman dashboard
             */
            return redirect('dashboard');

        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500, 
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    function logout(Request $request){
        /**
         * untuk menghapus sesi login/masuk sebagai 'web'
         */
        Auth::guard('web')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }

    function loginPage() {
        /**
         * untuk memvalidasi
         * dimana jika sudah login
         * akan di arahkan ke halaman dashboard
         */
        if (auth()->user()) {
            return redirect('dashboard');
        }
        return view('login');
    }

    function profile() {
        $item = User::find(auth()->user()->id);

        if (!$item) {
        return abort(404);
        }

        return view('user.edit', [
        'item' => $item,
        'profile' => true
        ]);
    }
}
